package com.wchua.learning.learning.customer;

record Customer(Integer id, String name, boolean subscribed) implements Comparable<Customer> {
    @Override
    public int compareTo(Customer o) {
        return id.compareTo(o.id);
    }
}
