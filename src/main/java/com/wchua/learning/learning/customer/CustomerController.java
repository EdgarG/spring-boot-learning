package com.wchua.learning.learning.customer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/customers")
@Slf4j
@RequiredArgsConstructor
@ExposesResourceFor(value = Customer.class)
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class CustomerController {

    final ConcurrentSkipListSet<Customer> customersData;

    @GetMapping
    CollectionModel<EntityModel<Customer>> customers() {
        return CollectionModel.of(customersData.stream().map(EntityModel::of).collect(Collectors.toList()));
    }

    @GetMapping("{id}")
    HttpEntity<EntityModel<Customer>> customer(@PathVariable(name = "id") Integer id) {
        return getCustomer(id)
                .map(c -> ResponseEntity.ok(EntityModel.of(c)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    private Optional<Customer> getCustomer(Integer id) {
        return customersData.stream().filter(c -> c.id().equals(id))
                .findFirst();
    }

    @PutMapping("{id}/unsubscribe")
    HttpEntity<EntityModel<Customer>> unsubscribe(@PathVariable(name = "id") Integer id) {
        changeCustomerSubscription(id, false);
        return getCustomer(id)
                .map(c -> ResponseEntity.ok(EntityModel.of(c)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("{id}/subscribe")
    HttpEntity<EntityModel<Customer>> subscribe(@PathVariable(name = "id") Integer id) {
        changeCustomerSubscription(id, true);
        return getCustomer(id)
                .map(c -> ResponseEntity.ok(EntityModel.of(c)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    private void changeCustomerSubscription(Integer id, boolean subscribed) {
        var list = customersData.stream()
                .map(c -> {
                    if(c.id().equals(id)){
                        return new Customer(c.id(), c.name(), subscribed);
                    }
                    return c;
                }).collect(Collectors.toList());
        customersData.clear();
        customersData.addAll(list);
    }

}

@Component
@RequiredArgsConstructor
@Slf4j
class Initializer implements CommandLineRunner {

    final ConcurrentSkipListSet<Customer> customersData;

    @Override
    public void run(String... args) throws Exception {
        customersData.add(new Customer(1, "Edgar", true));
        customersData.add(new Customer(2, "Wchua", false));
    }
}

@Component
@Slf4j
class BeansConfiguration {

    @Bean
    ConcurrentSkipListSet<Customer> customersData() {
        return new ConcurrentSkipListSet<>();
    }

}


@Component
@RequiredArgsConstructor
class CustomerRepresentationModelProcessor implements RepresentationModelProcessor<EntityModel<Customer>> {

    private final EntityLinks entityLinks;
    @Override
    public EntityModel<Customer> process(EntityModel<Customer> model) {
        var customer = model.getContent();
        var customerTypedEntityLinks = entityLinks.forType(Customer::id);
        var customerLink = customerTypedEntityLinks.linkForItemResource(customer);
        return model
                .add(customerLink.withSelfRel())
                .addIf(customer.subscribed(), () -> customerLink.slash("unsubscribe").withRel("unsubscribe"))
                .addIf(!customer.subscribed(), () -> customerLink.slash("subscribe").withRel("subscribe"))
                .add(entityLinks.linkToCollectionResource(Customer.class).withRel("customers"))
                ;
    }
}
