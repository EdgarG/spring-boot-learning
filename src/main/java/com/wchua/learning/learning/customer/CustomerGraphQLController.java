package com.wchua.learning.learning.customer;

import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SubscriptionMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Collection;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
class CustomerGraphQLController {
    final ConcurrentSkipListSet<Customer> customersData;

    @QueryMapping
    Collection<Customer> customers() {
        return customersData.stream().collect(Collectors.toList());
    }

    @SubscriptionMapping
    Flux<Customer> customersEvent() {
        return Flux.fromIterable(customersData)
                .delayElements(Duration.ofSeconds(1));
    }
}
